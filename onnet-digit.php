<?php

/**
 * @wordpress-plugin
 * Plugin Name:       The Digit
 * Plugin URI:        http://onnet.co.za/
 * Description:       The Digit creates a widgetized drag & drop interface to place components into page content & it also builds a widgetized page template builder.
 * Version:           1.1.2
 * Author:            OnNet
 * Author URI:        http://onnet.co.za/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       onnet-digit
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-onnet-digit-activator.php
 */
function activate_onnet_digit() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-onnet-digit-activator.php';
	Onnet_Digit_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-onnet-digit-deactivator.php
 */
function deactivate_onnet_digit() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-onnet-digit-deactivator.php';
	Onnet_Digit_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_onnet_digit' );
register_deactivation_hook( __FILE__, 'deactivate_onnet_digit' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-onnet-digit.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_onnet_digit() {

	$plugin = new Onnet_Digit();
	$plugin->run();

}
run_onnet_digit();
