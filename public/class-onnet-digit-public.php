<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://onnet.co.za/
 * @since      1.0.0
 *
 * @package    Onnet_Digit
 * @subpackage Onnet_Digit/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Onnet_Digit
 * @subpackage Onnet_Digit/public
 * @author     OnNet <code@onnet.co.za>
 */
class Onnet_Digit_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

		add_filter( 'the_digit', array( $this, 'the_digit') );
		//add_filter( 'the_content', array( $this, 'the_digit') );
	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {
	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() 
	{
	}

	public function the_digit( )
	{
		global $post;

		$digit = new OnNet_Digit_Display();
		$order = $digit->setup($post->ID);

		ob_start();
		$digit->display($order);
		$widgets = ob_get_clean();

		return $widgets;
	}
}