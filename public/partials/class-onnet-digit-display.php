<?php

function the_digit($post_id = false)
{
    if (!$post_id)
        $post_id = get_the_ID();

    $digit = new OnNet_Digit_Display();
    $order = $digit->setup($post_id);
    $digit->display($order);

}

class OnNet_Digit_Display
{
    public function setup($id, $cache = false)
    {
        // if no widget id is sent
        if (!isset($id) || $id === false)
            return;

        // Define the global digit variable
        global $digits;

        if (!is_array($digits))
            $digits = array('widgets' => array());

        $new_digits = $this->get_digits($id, $cache);

        if (!$new_digits) {
            $digits = false;
            return false;
        }
        // Update the global digit
        $digits[$id] = $new_digits['order'];

        // Add New widgets to array or make widgets the array.
        if (array_key_exists('widgets', $new_digits)):
            if (array_key_exists('widget', $digits))
                $digits['widgets'] = $digits['widgets'] + $new_digits['widgets'];
            else
                $digits['widgets'] = $new_digits['widgets'];
        endif;
        return $new_digits['order'];
    }

    public function display($order)
    {
        if (!is_array($order))
            return;

        global $digits;
        $widgets = $digits['widgets'];

        // ob_start();

        if (is_array($order)):
            foreach ($order as $id) {
                if (!array_key_exists($id, $widgets))
                    continue;

                $widget = $widgets[$id];

                if (!class_exists($widget['key']))
                    continue;

                $instance = (array)$widget['value'];

                if (is_array($instance)):

                    // Check if the widget is device locked
                    if ($this->device_locked($instance))
                        continue;

                    // Check if the widget is hyve locked
                    if ($this->hyve_lock($instance))
                        continue;

                endif;

                the_widget($widget['key'], $instance, false);
            }
        endif;

        //$widgets = ob_get_clean();

        //echo $widgets;
    }


    /**
     * Returns True if Locked else False.
     * @param $instance
     * @return bool
     */
    private function device_locked($instance)
    {
        if (!array_key_exists('ddd', $instance))
            return false;

        $lock = $instance['ddd'];

        if ($lock === 'desktop' && !is_desktop())
            return true;

        if ($lock === 'mobile' && is_desktop())
            return true;

        if( $lock === 'smart' && is_feature_phone()){
            return true;
        }
        
        if ($lock === 'feature' && !is_feature_phone())
            return true;

        return false;
    }

    private function billing_active()
    {
        static $active;

        if( isset($active) )
            return $active;

        $settings = apply_filters('api_credentials', true);

        if( !is_array($settings))
            $active = false;

        if( is_array($settings) && !array_key_exists('billing_active', $settings))
            $active = false;

        $active = true;

        return $active;
    }

    private function hyve_lock($instance)
    {
        if( !$this->billing_active() )
            return false;

        // If No Hyve Lock Continue.
        if (!array_key_exists('hyve_lock', $instance))
            return false;

        // Get Lock Key.
        $lock = $instance['hyve_lock'];

        // If lock key is no lock Continue.
        if ($lock === 'nolock')
            return false;

        // If Lock key is for subscribed && user logged in Continue
        if ($lock === 'subs' && is_user_logged_in())
            return false;

        // If Lock Key is for Unsubscribed && User not logged in Continue.
        if ($lock === 'unsub' && !is_user_logged_in())
            return false;

        // Else Lock it.
        return true;
    }

    private function get_digits($id, $cache = false)
    {
      //  if ($cache)
        //    $digits = $this->get_digits_from_cache($id);

        if (isset($digits) && $digits)
            return $digits;

        $digits = $this->get_digits_from_db($id);

      //  if ($cache)
        //    set_transient('digitcache_' . $id, $digits, 60 * 30);

        return $digits;
    }

    private function get_digits_from_cache($id)
    {
        return false; //get_transient('digitcache_' . $id);
    }

    private function get_digits_from_db($id)
    {
        global $wpdb;
        $digits = array();

        // digits sql
        $sql = "SELECT * FROM {$wpdb->prefix}digit WHERE post_id = {$id}";
        $results = $wpdb->get_results($sql);

        // If no digits.
        if (!is_array($results) || empty($results))
            return false;

        // Get Digit/Widget order
        $widget_order = $this->get_digits_order($results);

        // If no widget order found
        if (!$widget_order)
            return false;

        // Remove widget order from results & store value
        unset($results[$widget_order[0]]);
        $digits['order'] = $widget_order[1];

        foreach ($results as $digit) {
            $instance = (array)json_decode($digit->digit_value);

            foreach ($instance as $index => $row) {
                if (is_string($instance[$index]))
                    $instance[$index] = stripslashes($row);
            }

            $digits['widgets'][$digit->id] = array('key' => $digit->digit_key, 'value' => $instance);
        }


        return $digits;
    }

    private function get_digits_order($results)
    {
        $digits = false;

        foreach ($results as $key => $value) {
            if ($value->digit_key === 'widget_order') {
                $digits = array($key, json_decode($value->digit_value));
                break;
            }
        }

        return $digits;
    }

}