<?php

/**
 * Fired during plugin activation
 *
 * @link       http://onnet.co.za/
 * @since      1.0.0
 *
 * @package    Onnet_Digit
 * @subpackage Onnet_Digit/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Onnet_Digit
 * @subpackage Onnet_Digit/includes
 * @author     OnNet <code@onnet.co.za>
 */
class Onnet_Digit_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {
		Onnet_Digit_Admin::digit_table();
	}

}
