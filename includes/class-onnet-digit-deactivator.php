<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://onnet.co.za/
 * @since      1.0.0
 *
 * @package    Onnet_Digit
 * @subpackage Onnet_Digit/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Onnet_Digit
 * @subpackage Onnet_Digit/includes
 * @author     OnNet <code@onnet.co.za>
 */
class Onnet_Digit_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
