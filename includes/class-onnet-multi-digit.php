<?php

class onnet_digit_widget extends WP_Widget
{
    public $widget_category;
    public function __construct()
    {
        $widget_ops = array(
            'description' => 'Add an inner digit area to The Digit'
        );
        parent::__construct('onnet_digit_widget', '[Hyve] The Digit', $widget_ops);

        $this->widget_category = 'Digit';
    }

    public function widget($args, $instance)
    {
        $title = (!empty($instance['title'])) ? $instance['title'] : '';
        $title = apply_filters('title', $title, $instance, $this->id_base);

        $class = (!empty($instance['class'])) ? "class='" . $instance['class'] . "'"  : '';
        $class = apply_filters('class', $class, $instance, $this->id_base);

        $template = (!empty($instance['template'])) ? $instance['template'] : '';
        $template = apply_filters('template', $template, $instance, $this->id_base);

        $order = (!empty($instance['widgets'])) ? $instance['widgets'] : array();
        $order = apply_filters('widgets', $order, $instance, $this->id_base);

        $digit = new OnNet_Digit_Display();

        ob_start();
        $digit->display($order);
        $the_digit = ob_get_clean();

        if (locate_template($template))
            include locate_template($template);
        else
            echo "<div {$class}>{$the_digit}</div>";

    }


    public function get_widget_templates()
    {
        $wp_theme = wp_get_theme();
        $widget_templates = array();
        $files = (array)$wp_theme->get_files('php', 1);

        foreach ($files as $file => $full_path) {
            if (!preg_match('|Widget Template:(.*)$|mi', file_get_contents($full_path), $header)) {
                continue;
            }
            $widget_templates[$file] = _cleanup_header_comment($header[1]);
        }

        return $widget_templates;
    }


    public function form($instance)
    {
        if (isset($instance['title'])) {
            $title = $instance['title'];
        } else {
            $title = false;
        }
        if (isset($instance['class'])) {
            $class = $instance['class'];
        } else {
            $class = false;
        }
        if (isset($instance['template'])) {
            $template = $instance['template'];
        } else {
            $template = false;
        }
        if (isset($instance['widgets'])) {
            $widgets = $instance['widgets'];
        } else {
            $widgets = array();
        }

        $templates = get_widget_template('Multi Digit');

        ?>
        <p>
            <label>Digit Title:</label>
            <input class="widefat" type="text" name="<?php echo $this->get_field_name('title'); ?>"
                   id="<?php echo $this->get_field_id('title'); ?>" value="<?php echo $title; ?>">
        </p>
        <p>
            <label>Container Class:</label>
            <input class="widefat" type="text" name="<?php echo $this->get_field_name('class'); ?>"
                   id="<?php echo $this->get_field_id('class'); ?>" value="<?php echo $class; ?>">
        </p>

        <p>Drag your widgets here</p>
        <ul class="digit_area drop_zone" id="d<?php echo $this->id; ?>">
        </ul>


        <p>
            <label>Template:</label>
            <select name="<?php echo $this->get_field_name('template'); ?>"
                    id="<?php echo $this->get_field_id('template'); ?>">

                <option><?php echo _e('Default'); ?></option>
                <?php
                if (is_array($templates)) {
                    foreach ($templates as $template_location => $template_name) {
                        $selected = '';
                        if ($template == $template_location)
                            $selected = 'selected';

                        echo '<option value="' . $template_location . '" ' . $selected . '>' . $template_name . '</option>';

                    }
                    wp_reset_postdata();
                }
                ?>
            </select>
        </p>
        <?php
    }
}

add_action('widgets_init', 'load_onnet_digit_widget');
function load_onnet_digit_widget()
{
    global $pagenow;

    if ($pagenow !== 'widgets.php')
        register_widget('onnet_digit_widget');

}