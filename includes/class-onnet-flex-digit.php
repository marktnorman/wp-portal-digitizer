<?php

class onnet_flex_digit extends WP_Widget
{
    public $widget_category;
    public function __construct()
    {
        $widget_ops = array(
            'description' => "This is the placement of a page/post's digits."
        );
        parent::__construct('onnet_flex_digit', "[Hyve] Page Digits", $widget_ops);

        $this->widget_category = 'Digit';
    }

    public function widget($args, $instance)
    {

        echo apply_filters('the_digit', true);
    }

    public function form($instance)
    {
       echo '<p>There are no editable fields for this widget.</p>';
    }

    public function update($new_instance, $old_instance){
        return $new_instance;
    }
}

add_action('widgets_init', 'load_onnet_flex_digit');
function load_onnet_flex_digit()
{
    global $pagenow;

    if ($pagenow !== 'widgets.php')
        register_widget('onnet_flex_digit');

}