<?php


class Custom_Digit_Post_Type
{
    public $post_type_name;
    public $post_type_args;
    public $post_type_labels;
    private $plural;

    /* Class constructor */
    public function __construct($name, $args = array(), $labels = array(), $plural = true)
    {

        // Set some important variables
        if (!ctype_upper($name))
            $name = strtolower($name);

        $this->post_type_name = str_replace(' ', '-', $name);
        $this->post_type_args = $args;
        $this->post_type_labels = $labels;
        $this->plural = $plural;
        // Add action to register the post type, if the post type does not already exist
        if (!post_type_exists($this->post_type_name)) {
            $this->register_post_type();
        }

        // Listen for the save post hook
        //$this->save();
        add_action('save_post', array(&$this, 'save_data'));
    }

    /* Method which registers the post type */
    public function register_post_type()
    {
        //Capitilize the words and make it plural
        $name = ucwords(str_replace('-', ' ', $this->post_type_name));
        if ($this->plural) {
            $plural = $this->get_plural($name);
        } else {
            $plural = $name;
        }

        // We set the default labels based on the post type name and plural. We overwrite them with the given labels.
        $labels = array_merge(

        // Default
            array(
                'name' => _x($plural, 'post type general name', 'tracetv'),
                'singular_name' => _x($plural, 'post type singular name', 'tracetv'),
                'add_new' => _x('Add New', strtolower($name), 'tracetv'),
                'add_new_item' => __('Add New ' . $name, 'tracetv'),
                'edit_item' => __('Edit ' . $name, 'tracetv'),
                'new_item' => __('New ' . $name, 'tracetv'),
                'all_items' => __('All ' . $plural, 'tracetv'),
                'view_item' => __('View ' . $name, 'tracetv'),
                'search_items' => __('Search ' . $name, 'tracetv'),
                'not_found' => __('No ' . strtolower($name) . ' found', 'tracetv'),
                'not_found_in_trash' => __('No ' . strtolower($name) . ' found in Trash', 'tracetv'),
                'parent_item_colon' => '',
                'menu_name' => __($plural, 'tracetv')
            ),

            // Given labels
            $this->post_type_labels

        );

        // Same principle as the labels. We set some defaults and overwrite them with the given arguments.
        $args = array_merge(

        // Default
            array(
                'label' => $plural,
                'labels' => $labels,
                'public' => true,
                'show_ui' => true,
                'supports' => array('title'),
                'show_in_nav_menus' => true,
                '_builtin' => false,
            ),

            // Given args
            $this->post_type_args

        );

        // Register the post type
        $name = str_replace('-&-', '-', $this->post_type_name);
        register_post_type($name, $args);
    }

    /* Method to attach the taxonomy to the post type */
    public function add_taxonomy($name, $args = array(), $labels = array())
    {


        if (!empty($name)) {

            // We need to know the post type name, so the new taxonomy can be attached to it.
            $post_type_name = strtolower(str_replace('-&-', '-', $this->post_type_name));

            // Taxonomy properties
            $taxonomy_name = str_replace(' ', '_', $name);
            $taxonomy_name = str_replace('_&', '', $taxonomy_name);
            $taxonomy_name = strtolower($taxonomy_name);

            $label_name = str_ireplace(str_replace('-', ' ', $this->post_type_name . ' '), '', $name);

            $taxonomy_labels = $labels;
            $taxonomy_args = $args;

            if (!taxonomy_exists($taxonomy_name)) {
                /* Create taxonomy and attach it to the object type (post type) */
                //Capitilize the words and make it plural
                $name = $label_name;
                $plural = $this->get_plural($name);

                // Default labels, overwrite them with the given labels.
                $labels = array_merge(

                // Default
                    array(
                        'name' => _x($plural, 'taxonomy general name', 'kaizerchiefs'),
                        'singular_name' => _x($name, 'taxonomy singular name', 'kaizerchiefs'),
                        'search_items' => __('Search ' . $name, 'kaizerchiefs'),
                        'all_items' => __('All ' . $name, 'kaizerchiefs'),
                        'parent_item' => __('Parent ' . $name, 'kaizerchiefs'),
                        'parent_item_colon' => __('Parent ' . $name . ':', 'kaizerchiefs'),
                        'edit_item' => __('Edit ' . $name, 'kaizerchiefs'),
                        'update_item' => __('Update ' . $name, 'kaizerchiefs'),
                        'add_new_item' => __('Add New ' . $name, 'kaizerchiefs'),
                        'new_item_name' => __('New ' . $name . ' Name', 'kaizerchiefs'),
                        'menu_name' => __($plural, 'kaizerchiefs')
                    ),

                    // Given labels
                    $taxonomy_labels

                );

                // Default arguments, overwritten with the given arguments
                $args = array_merge(

                // Default
                    array(
                        'label' => $name,
                        'labels' => $labels,
                        'hierarchical' => true,
                        'public' => true,
                        'show_ui' => true,
                        'show_in_nav_menus' => true,
                        '_builtin' => false
                    ),

                    // Given
                    $taxonomy_args

                );

                // Add the taxonomy to the post type


                register_taxonomy($taxonomy_name, $post_type_name, $args);

            } else {
                /* The taxonomy already exists. We are going to attach the existing taxonomy to the object type (post type) */


                register_taxonomy_for_object_type($taxonomy_name, $post_type_name);


            }
        }
    }

    private function get_plural($name)
    {
        $last = strlen($name) - 1;
        if ($name[$last] === 'y') {
            $plural = substr($name, 0, $last);
            $plural .= 'ies';
        } elseif ($name[$last] === 'y') {
            $plural = $name;
        } else {
            $plural = $name . 's';
        }

        return $plural;
    }

    /* Attaches meta boxes to the post type */
    public function add_meta_box($title, $fields = array(), $context = 'normal', $priority = 'default')
    {


        if (!empty($title)) {
            // We need to know the Post Type name again
            $post_type_name = $this->post_type_name;

            // Meta variables
            $box_id = strtolower(str_replace(' ', '_', $title));
            $box_title = ucwords(str_replace('_', ' ', $title));
            $box_context = $context;
            $box_priority = $priority;

            // Make the fields global
            global $custom_fields;

            $custom_fields[$title] = $fields;

            $media_buttons = array();

            /* More code coming */
            add_action('admin_init',
                function () use ($box_id, $box_title, $post_type_name, $box_context, $box_priority, $fields) {
                    add_meta_box(
                        $box_id,
                        $box_title,
                        function ($post, $data) {
                            global $post;
                            ?>

                            <?php
                            // Nonce field for some validation
                            wp_nonce_field(plugin_basename(__FILE__), 'custom_post_type');

                            // Get all inputs from $data
                            $custom_fields = $data['args'][0];

                            // Get the saved values
                            $meta = get_post_custom($post->ID);


                            // Check the array and loop through it
                            if (!empty($custom_fields)) {
                                /* Loop through $custom_fields */
                                foreach ($custom_fields as $label => $type) {

                                    $field_id_name = strtolower(str_replace(' ', '_', $data['id'])) . '_' . strtolower(str_replace(' ', '_', $label));

                                    $meta_value = "";
                                    if (isset($meta[$field_id_name][0]) && $meta[$field_id_name][0] != "") {
                                        $meta_value = $meta[$field_id_name][0];
                                    }

                                    switch ($type['input']) {
                                        case 'featured':

                                            echo '<p class="hide-if-no-js" width="100%">';
                                            echo '<a title="Set featured image" href="#" id="' . $field_id_name . '_btn" class="featured_image">';
                                            if ($meta_value) {
                                                // get image source
                                                $image_src = wp_get_attachment_url($meta_value);
                                                echo '<img src ="' . $image_src . '" id="' . $field_id_name . '_src" width="100%" height="auto">';
                                                echo '<span style="display:none;" id="' . $field_id_name . '_text">Set Featured Image</span>';
                                                echo '</a></p>';
                                                echo '<p class="hide-if-no-js" id="' . $field_id_name . '_del" style="margin-bottom: 1px!important;"><a href="#" class="featured_remove" id="' . $field_id_name . '_remove">Remove featured image</a></p>';
                                            } else {
                                                echo '<img src ="" id="' . $field_id_name . '_src" width="100%" height="auto">';
                                                echo '<span id="' . $field_id_name . '_text">Set Featured Image</span>';
                                                echo '</a></p>';
                                                echo '<p class="hide-if-no-js" id="' . $field_id_name . '_del" style="display:none; margin-bottom: 1px!important;"><a href="#" class="featured_remove" id="' . $field_id_name . '_remove">Remove featured image</a></p>';
                                            }
                                            echo '<input style="display:none" type="text" name="custom_meta[' . $field_id_name . ']" id="' . $field_id_name . '" value="' . $meta_value . '" class="widefat" />';


                                            break;
                                        case 'text':
                                            echo '<div class="onnet-meta">';
                                            echo '<div class="onnet-meta-label"><label for="' . $field_id_name . '">' . $type['label'] . '</label><p>' . $type['description'] . '</p></div>';
                                            echo '<div class="onnet-meta-content"><input type="text" name="custom_meta[' . $field_id_name . ']" id="' . $field_id_name . '" value="' . $meta_value . '" class="widefat" /></div>';
                                            echo '</div>';
                                            break;
                                        case 'textarea':
                                            echo '<div class="onnet-meta">';
                                            echo '<div class="onnet-meta-label"><label for="' . $field_id_name . '">' . $type['label'] . '</label><p>' . $type['description'] . '</p></div>';
                                            echo '<div class="onnet-meta-content"><textarea name="custom_meta[' . $field_id_name . ']" id="' . $field_id_name . '">' . $meta_value . '</textarea></div>';
                                            echo '</div>';
                                            break;
                                        case 'select':
                                            echo '<div class="onnet-meta">';
                                            echo '<div class="onnet-meta-label"><label for="' . $field_id_name . '">' . $type['label'] . '</label><p>' . $type['description'] . '</p></div>';
                                            echo '<div class="onnet-meta-content"><select class="widefat" name="custom_meta[' . $field_id_name . ']" id="' . $field_id_name . '">';
                                            foreach ($type['options'] as $option => $value) {
                                                $selected = '';
                                                if (isset($meta[$field_id_name][0]) && $meta[$field_id_name][0] == $option) {
                                                    $selected = 'selected="selected"';
                                                }
                                                echo '<option ' . $selected . ' value="' . $option . '">' . $value . '</option>';
                                            }
                                            echo '</select></div>';
                                            echo '</div>';
                                            break;
                                        case 'radio':
                                            echo '<div class="onnet-meta">';
                                            echo '<div class="onnet-meta-label"><label for="' . $field_id_name . '">' . $type['label'] . '</label><p>' . $type['description'] . '</p></div>';
                                            echo '<div class="onnet-meta-content radio">';
                                            foreach ($type['options'] as $option => $value) {
                                                $checked = '';
                                                if (isset($meta[$field_id_name][0]) && $meta[$field_id_name][0] == $option) {
                                                    $checked = 'checked="checked"';
                                                }
                                                echo '<label><input type="radio" name="custom_meta[' . $field_id_name . ']" id="' . $field_id_name . '" value="' . $option . '" ' . $checked . ' />' . $value . '</label><br><br>';
                                            }
                                            echo '</div>';
                                            echo '</div>';
                                            break;
                                        case 'checkbox':
                                            echo '<div class="onnet-meta">';
                                            echo '<div class="onnet-meta-label"><label for="' . $field_id_name . '">' . $type['label'] . '</label><p>' . $type['description'] . '</p></div>';
                                            echo '<div class="onnet-meta-content checkbox">';
                                            foreach ($type['options'] as $option => $value) {
                                                $custom_meta = get_post_meta($post->ID, $field_id_name, true);
                                                if (is_array($custom_meta) && in_array($option, $custom_meta)) {
                                                    $checked = 'checked="checked"';
                                                } else {
                                                    $checked = '';
                                                }
                                                echo '<label ><input type="checkbox" name="' . $field_id_name . '[]" value="' . $option . '" ' . $checked . ' />' . $value . '</label>';
                                            }
                                            wp_reset_postdata();
                                            echo '</div>';
                                            echo '</div>';
                                            break;
                                        case 'taxonomy':
                                            $taxonomy_list = get_terms($type['taxonomy'], "orderby=count&hide_empty=0");
                                            echo '<div class="onnet-meta">';
                                            echo '<div class="onnet-meta-label"><label for="' . $field_id_name . '">' . $type['label'] . '</label><p>' . $type['description'] . '</p></div>';
                                            echo '<div class="onnet-meta-content">';
                                            echo '<select name="custom_meta[' . $field_id_name . ']" id="' . $field_id_name . '">';
                                            foreach ($taxonomy_list as $tax) {
                                                $use_value = $tax->name;
                                                if ($use_value == $meta[$field_id_name][0]) {
                                                    $selected = 'selected="selected"';
                                                } else {
                                                    $selected = "";
                                                }
                                                echo '<option ' . $selected . ' value="' . $use_value . '">' . $tax->name . '</option>';
                                            }
                                            echo '</select>';
                                            echo '</div>';
                                            echo '</div>';
                                            break;
                                        case 'editor':
                                            $editor_id = $field_id_name;
                                            $meta_key = $type['name'];
                                            $content = get_post_meta($post->ID, $meta_key, true);
                                            $settings = array(
                                                'media_buttons' => true,
                                                'tinymce' => array(
                                                    'height' => 400
                                                )
                                            );
                                            wp_editor($content, $field_id_name, $settings);
                                            break;
                                        case 'slide':
                                            $post_slides = get_post_meta($post->ID, 'post_slides', true);

                                            $max_slides = (!empty($post_slides) && 0 != count($post_slides['excerpt'])) ? count($post_slides['excerpt']) : $max = 1;
                                            ?>

                                            <div id="postcustomstuff" class="slider-list-meta">
                                                <p>
                                                    Enter details below. Drag and Drop them to order them.
                                                    <input type="button" name="cpts-new-slide-button"
                                                           class="alignright button"
                                                           value="<?php echo __('Add Slide'); ?>"/>
                                                </p>

                                                <table id="slider-meta">
                                                    <thead>
                                                    <tr>
                                                        <th class="center"><label><?php echo __('Image'); ?></label>
                                                        </th>
                                                        <th class="center">
                                                            <label><?php echo __('Excerpt'); ?></label>
                                                        </th>
                                                        <th></th>
                                                    </tr>
                                                    </thead>

                                                    <tbody class="sortable ui-sortable">
                                                    <?php

                                                    for ($i = 0; $i < $max_slides; $i++) {
                                                        if (isset($post_slides['thumbnail'][$i]) && '' != $post_slides['thumbnail'][$i]) {
                                                            $img = wp_get_attachment_image_src($post_slides['thumbnail'][$i], 'thumbnail');
                                                            $img = $img[0];
                                                            $imgclass = "";
                                                        } else {
                                                            $img = "";
                                                            $imgclass = "placeholder";
                                                        }

                                                        $slide_thumbnail = (isset($post_slides['thumbnail'][$i])) ? $post_slides['thumbnail'][$i] : '';
                                                        $slide_excerpt = (isset($post_slides['excerpt'][$i])) ? $post_slides['excerpt'][$i] : '';
                                                        ?>
                                                        <tr class="psc-admin-slide-element">
                                                            <td>
                                                                <img src="<?php echo $img; ?>"
                                                                     class="slide-image <?php echo $imgclass; ?>"/>
                                                                <a href="#"
                                                                   class="cpts_upload_image_button slide-button"><?php echo __('Upload'); ?></a>
                                                                <a href="#"
                                                                   class="cpts_clear_image_button delete slide-button"><?php echo __('Remove'); ?></a>
                                                                <input type="hidden" id="slide_thumbnail"
                                                                       name="slide[thumbnail][]"
                                                                       value="<?php echo $slide_thumbnail; ?>"/>
                                                            </td>
                                                            <td class="left tagchecklist">
																<textarea id="slide[excerpt][]" name="slide[excerpt][]"
                                                                          rows="7"
                                                                          cols="25"><?php echo $slide_excerpt; ?></textarea>
                                                            </td>
                                                            <td width="5%">
                                                                    <span class="delete-slide"><a
                                                                            class="cpts_delete-slide">Delete</a></span>
                                                            </td>
                                                        </tr>
                                                    <?php } ?>
                                                    </tbody>
                                                </table>
                                            </div>

                                            <script type="text/javascript">
                                                jQuery(document).ready(function ($) {
                                                    $('div.slider-list-meta').post_slider_admin();

                                                    $('table#slider-meta tbody').sortable({
                                                        placeholder: "border",
                                                        start: function (event, ui) {
                                                            // change css here
                                                            ui.item.toggleClass("drag");
                                                        },
                                                        stop: function (event, ui) {
                                                            // change css here
                                                            ui.item.toggleClass("drag");
                                                        }
                                                    });
                                                });
                                            </script>
                                            <?php
                                            break;

                                    }
                                }
                            }

                        },
                        $post_type_name,
                        $box_context,
                        $box_priority,
                        array($fields)
                    );
                }
            );


            add_action('admin_enqueue_scripts', function () use ($media_buttons) {

                wp_enqueue_media();
                // Media Uploader
                wp_enqueue_script('media-upload');


                wp_enqueue_script('image-upload', get_template_directory_uri() . "/assets/js/media_upload.js");

            });
        }

    }

    public function save_data()
    {
        global $post;
        // Deny the Wordpress autosave function
        if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
            return;
        }

        if (isset($_POST['custom_post_type'])) {
            if (!wp_verify_nonce($_POST['custom_post_type'], plugin_basename(__FILE__))) {
                return;
            }
        }


        if (isset($_POST) && isset($post->ID) && get_post_type($post->ID) == $this->post_type_name) {
            global $custom_fields;

            // Loop through each meta box
            foreach ($custom_fields as $title => $fields) {
                // Loop through all fields
                foreach ($fields as $label => $type) {

                    $field_id_name = strtolower(str_replace(' ', '_', $title)) . '_' . strtolower(str_replace(' ', '_', $label));

                    if ($type['input'] == 'editor') {

                        $editor_id = $field_id_name;
                        $meta_key = $type['name'];
                        if (isset($_REQUEST[$editor_id])) {
                            update_post_meta($_REQUEST['post_ID'], $meta_key, $_REQUEST[$editor_id]);
                        }

                    } elseif ($type['input'] == 'checkbox') {

                        if (isset($_POST[$field_id_name])) {
                            $custom = $_POST[$field_id_name];
                            update_post_meta($post->ID, $field_id_name, $custom);
                        } else {
                            update_post_meta($post->ID, $field_id_name, '');
                        }

                    } elseif ($type['input'] == 'slide') {
                        if (isset($_POST['slide'])) {
                            update_post_meta($post->ID, 'post_slides', $_POST['slide']);
                        }

                    } else {
                        if (isset($_POST['custom_meta'][$field_id_name])) {
                            update_post_meta($post->ID, $field_id_name, $_POST['custom_meta'][$field_id_name]);
                        }
                    }
                }
            }
        }
    }

    public static function beautify($string)
    {
        return ucwords(str_replace('_', ' ', $string));
    }

    public static function uglify($string)
    {
        return strtolower(str_replace(' ', '_', $string));
    }
}


class Digit_Tizer
{

    private $tizer;

    public function __construct($id = false)
    {
        $this->setup($id);
    }

    public function display()
    {
        global $wpdb;

        $digit_order = "SELECT digit_value FROM {$wpdb->prefix}digit WHERE post_id = {$this->tizer->ID} AND digit_key = 'widget_order'";

        $digit_order = (array)$wpdb->get_row($digit_order);

        if (empty($digit_order))
            return;
        else
            $digit_order = json_decode($digit_order['digit_value']);

        $get_digits = "SELECT * FROM {$wpdb->prefix}digit WHERE post_id = {$this->tizer->ID}";
        $digits = $wpdb->get_results($get_digits, 'OBJECT_K');

        ob_start();

        if (is_array($digit_order)):
            foreach ($digit_order as $digit_id) {
                $widget = $digits[$digit_id];

                $instance = (array)json_decode($widget->digit_value);

                if (is_array($instance)):
                    // Check if the widget is device locked
                    if ($this->device_locked($instance))
                        continue;

                    // Check if the widget is hyve locked
                    if ($this->hyve_lock($instance))
                        continue;

                    foreach ($instance as $index => $row):
                        if (is_string($instance[$index])):
                            $instance[$index] = stripslashes($row);
                        endif;
                    endforeach;

                endif;


                the_widget($widget->digit_key, $instance, false);
            }
        endif;

        $widgets = ob_get_clean();

        echo $widgets;
    }


    private function device_locked($instance)
    {
        if (!array_key_exists('ddd', $instance))
            return false;

        $lock = $instance['ddd'];

        if ($lock === 'desktop' && !is_desktop())
            return true;

        if ($lock === 'mobile' && is_desktop())
            return true;

        if ($lock === 'smart' && !is_smart_phone())
            return true;

        if ($lock === 'feature' && !is_feature_phone())
            return true;

        return false;
    }

    private function billing_active()
    {
        static $active;

        if( isset($active) )
            return $active;

        $settings = apply_filters('api_credentials', true);

        if( !is_array($settings))
            $active = false;

        if( !array_key_exists('billing_active', $settings))
            $active = false;

        $active = true;

        return $active;
    }

    private function hyve_lock($instance)
    {
        if( !$this->billing_active() )
            return false;
        // If No Hyve Lock Continue.
        if (!array_key_exists('hyve_lock', $instance))
            return false;

        // Get Lock Key.
        $lock = $instance['hyve_lock'];

        // If lock key is no lock Continue.
        if ($lock === 'nolock')
            return false;

        // If Lock key is for subscribed && user logged in Continue
        if ($lock === 'subs' && is_user_logged_in())
            return false;

        // If Lock Key is for Unsubscribed && User not logged in Continue.
        if ($lock === 'unsub' && !is_user_logged_in())
            return false;

        // Else Lock it.
        return true;
    }

    private function setup($id)
    {
        global $tizer;

        $tizer = new stdClass();
        $tizer->template = plugin_dir_path(__FILE__) . 'tizer-template.php';
        $tizer->ID = $id;

        $this->tizer = $tizer;
    }

    public function admin()
    {
        if (class_exists('Custom_Digit_Post_Type')) {
            new Custom_Digit_Post_Type('Digitizer',
                array(
                    'has_archive' => false,
                    'publicly_queryable' => false,
                    'exclude_from_search' => true,
                    'query_var' => false,
                    'supports' => array(
                        'title',
                    )
                )
            );
        } else {
            return false;
        }
    }
}

