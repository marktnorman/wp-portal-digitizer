<?php
/**
 *    Widget load init
 */
add_action('wp_ajax_widgets_init', 'widgets_init');
add_action('wp_ajax_nopriv_widgets_init', 'widgets_init');
function widgets_init()
{
    // Variables
    if (isset($_POST['post_id']) && !empty($_POST['post_id'])) {
        $post_id = $_POST['post_id'];
    } else {
        print_r(json_encode(false));
        exit;
    }

    $widgets_class = new OnNet_Widget_Order($post_id);

    // Get Widget Order
    $widgets_order = $widgets_class->get_order();

    // If no widgets return false
    if (!$widgets_order) {
        print_r(json_encode(false));
        exit;
    }

    // Get Admin Widget Data
    $widgets = $widgets_class->admin_init($widgets_order);

    // Return widgets
    print_r(json_encode($widgets));
    exit;
}

/**
 *    Create Widget Ajax Trigger
 */
add_action('wp_ajax_get_widget', 'get_widget');
add_action('wp_ajax_nopriv_get_widget', 'get_widget');
function get_widget()
{
    global $wpdb;
    $widget_id = $_POST['widget_id'];

    $widget = $wpdb->get_row("SELECT * FROM {$wpdb->prefix}digit WHERE id = {$widget_id}");

    $widget = new OnNet_Widget($widget->post_id, $widget->digit_key, $widget->id,
        (array) json_decode($widget->digit_value));

    if ($widget) {
        print_r(json_encode($widget->get_widget_data()));
        exit;
    }
}

/**
 *    Create Widget Ajax Trigger
 */
add_action('wp_ajax_create_widget', 'create_widget');
add_action('wp_ajax_nopriv_create_widget', 'create_widget');
function create_widget()
{
    $widget = new OnNet_Widget($_POST['post_id'], $_POST['widget'], false);
    $widget_data = $widget->get_widget_data();
    print_r(json_encode($widget_data));
    exit;
}

/**
 *    Update Widget
 */
add_action('wp_ajax_update_widget', 'update_widget');
add_action('wp_ajax_nopriv_update_widget', 'update_widget');
function update_widget()
{

    $post_id = $_POST['post_id'];
    $widget_key = $_POST['widget'];
    $widget_id = $_POST['widget_id'];

    // Get Form Data
    parse_str($_POST['data'], $form_data);
    $instance = $form_data[$widget_id];

    // Update widget & get new form
    $widget = new OnNet_Widget($post_id, $widget_key, $widget_id, $instance);
    $new_form = $widget->update($instance);

    // Clear Cache
    //delete_transient('digitcache_' . $post_id);

    // Return Form.
    print_r(json_encode($new_form));
    exit;
}

/**
 *    Delete Widget
 */
add_action('wp_ajax_delete_widget', 'delete_widget');
add_action('wp_ajax_nopriv_delete_widget', 'delete_widget');
function delete_widget()
{
    $post_id = $_POST['post_id'];

    $widget = new OnNet_widget($post_id, false, $_POST['widget'], false);
    $delete = $widget->delete();

    // Clear Cache
    //delete_transient('digitcache_' . $post_id);

    print_r(json_encode($delete));
    exit;
}

/**
 *    Update Widget Order Ajax Trigger
 */
add_action('wp_ajax_widget_order', 'update_widget_order');
add_action('wp_ajax_nopriv_widget_order', 'update_widget_order');
function update_widget_order()
{
    $post_id = $_POST['post_id'];
    $widgets = $_POST['widgets'];
    $widget_area = $_POST['widget_area'];

    if ($widget_area === '#digit_sortable') {
        $widgets = new OnNet_Widget_Order($post_id, $widgets);
        $update = $widgets->update_order();
    } else {
        $widget_area = str_replace('#d', '', $widget_area);
        $widget = new OnNet_Widget($post_id, 'onnet_digit_widget', $widget_area);
        $widget->update_digit_order($widgets);
    }

    // Clear Cache
    //delete_transient('digitcache_' . $post_id);

    print_r(json_encode($update));
    exit;
}

/**
 *    digits_init
 */
add_action('wp_ajax_digits_init', 'digits_init');
add_action('wp_ajax_nopriv_digits_init', 'digits_init');
function digits_init()
{
    $post_id = $_POST['post_id'];
    $digit_id = $_POST['digit_id'];

    $digit = new OnNet_Widget($post_id, 'onnet_digit_widget', $digit_id);
    $digit = $digit->get_instance();

    $widgets_order = $digit['widgets'];

    // If no widgets return false
    if (!$widgets_order) {
        print_r(json_encode(false));
        exit;
    }

    $widgets_class = new OnNet_Widget_Order($post_id);
    $widgets = $widgets_class->admin_init($widgets_order);

    // Return widgets
    print_r(json_encode($widgets));
    exit;
}
