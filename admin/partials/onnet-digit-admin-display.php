<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://onnet.co.za/
 * @since      1.0.0
 *
 * @package    Onnet_Digit
 * @subpackage Onnet_Digit/admin/partials
 */
