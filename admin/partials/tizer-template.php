<?php

if( function_exists('get_portal_header'))
    get_portal_header();
else
    get_header();

if( class_exists('Digit_Tizer')):
    global $tizer;
    
    $digit = new OnNet_Digit_Display();
    $order = $digit->setup($tizer->ID, true);
    $digit->display($order);

else:

    echo 'No template layout found';

endif;

if( function_exists('get_portal_footer'))
    get_portal_footer();
else
    get_footer();