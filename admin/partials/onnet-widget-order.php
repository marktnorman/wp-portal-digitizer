<?php

class OnNet_Widget_Order
{

    // Post ID
    private $post_id;

    // Widget Order
    private $widgets;

    // Widget Order ID
    private $order_id;

    // Widget Order Key
    private $key = 'widget_order';

    // Database Table.
    private $table = 'digit';

    public function __construct($post_id, $widgets = array())
    {
        $this->post_id = $post_id;
        $this->widgets = $widgets;
        $this->order_id = $this->get_widget_order_id();
    }

    /**
     *    Public Method: Get Order
     *    Returns the order of widgets saved in the database for the post save.
     */
    public function get_order()
    {
        // main variables
        global $wpdb;

        // If there is no post id. Return false.
        if(empty($this->post_id) || !isset($this->post_id)){
            return false;
        }

        // Query vars
        $table = $wpdb->prefix . $this->table;
        $query = "SELECT digit_value FROM {$table} WHERE post_id = {$this->post_id} AND digit_key = '{$this->key}'";

        // Get Result
        $result = $wpdb->get_row($query);

        // If no resut return false(empty)
        if (!$result)
            return false;

        // Decode order to get widget keys
        $widget_ids = json_decode($result->digit_value);

        // If keys are empty return false;
        if (empty($widget_ids))
            return false;

        return $widget_ids;
    }

    public function admin_init($widget_ids)
    {
        global $wpdb;

        $widgets = array();

        // Get widgets by id
        $table = $wpdb->prefix . $this->table;
        $widget_ids_Q = implode(', ', $widget_ids);
        $_widgets = $wpdb->get_results("SELECT * FROM {$table} WHERE id IN ({$widget_ids_Q})", 'OBJECT_K');

        // IF widgets false or empty return false.
        if (!$_widgets || empty($_widgets))
            return false;

        // Unset deleted widgets from widget order ids.
        foreach ($widget_ids as $index => $widget_id)
            if (!array_key_exists($widget_id, $_widgets))
                unset($widget_ids[$index]);

        $widget_ids = array_values($widget_ids);

        foreach ($_widgets as $_widget) {
            // If the widget class exists
            if (class_exists($_widget->digit_key)) {
                // Get Instance & Instantiate the Widget.
                $instance = json_decode(($_widget->digit_value));
                $widget = new OnNet_Widget($_widget->post_id, $_widget->digit_key, $_widget->id, (array)$instance);
                $widgets[$_widget->id] = $widget->get_widget_data();
            } else {
                // If Widget Doesn't exist. Remove from widget order.
                if (($index = array_search($_widget->id, $widget_ids)) !== false)
                    unset($widget_ids[$index]);

                // Delete the widget row
                $delete = array('id' => $_widget->id);
                $delete = $wpdb->delete($table, $delete);
            }
        }

        return array('order' => $widget_ids, 'widget' => $widgets);
    }

    /**
     *    Public Method: Update Order.
     *    Updates the order of the posts widget.
     */
    public
    function update_order($widgets = false)
    {
        global $wpdb;

        if ($widgets)
            $this->widgets = $widgets;

        if ($this->widgets === null || $this->widgets === '')
            $this->widgets = array();

        $table = $wpdb->prefix . $this->table;
        $update = array('digit_value' => json_encode(array_filter($this->widgets)));
        $where = array('id' => $this->order_id);

        $update = $wpdb->update($table, $update, $where);

        return $update;
    }


    /**
     *    Private Method: Get Widget Order ID
     *    This returns the widget order database id.
     *    Creates initital row if not set.
     */
    private
    function get_widget_order_id()
    {
        global $wpdb;

        // If there is no post id. Return false.
        if(empty($this->post_id) || !isset($this->post_id)){
            return false;
        }


        $sql = "SELECT id from {$wpdb->prefix}{$this->table} WHERE post_id = {$this->post_id} AND digit_key = '{$this->key}'";
        $row = $wpdb->get_row($sql);

        // If row is set
        if (isset($row))
            return $row->id;

        // Insert initial row - set data
        $data = array(
            'digit_key' => $this->key,
            'post_id' => $this->post_id,
            'digit_value' => json_encode($this->widgets),
        );

        // Insert row
        $wpdb->insert($wpdb->prefix . $this->table, $data);

        // Return the id;
        return $wpdb->insert_id;
    }


}

