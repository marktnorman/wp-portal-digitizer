<?php

class Onnet_widgets
{

    public function __construct()
    {
        add_action('widgets_init', array($this, 'unregister_widgets'), '100');
        add_action('add_meta_boxes', array($this, 'digit_metaboxes'));
    }

    public function digit_metaboxes()
    {
        $options = get_option('onnet_digit_options');

        if (!empty($options['digit_post_types'])) {
            foreach ($options['digit_post_types'] as $screen) {
                add_meta_box('digit_area', '[Hyve] Widget Area', array($this, 'digit_area'), $screen, 'normal', 'high');
                // add_meta_box('widget_list', '[Hyve] Widgets', array($this, 'widget_list'), $screen, 'normal', 'high');

                $widget_list = $this->get_widget_lists();

                if (is_array($widget_list)):
                    foreach ($widget_list as $key => $widgets):
                        $box_id = str_replace('-', '_', str_replace(' ', '_', strtolower($key)));
                        // Inherit $message
                        add_meta_box($box_id, '[Hyve] ' . $key, function () use ($widgets, $box_id, $key, $screen) {
                            ?>
                            <p>Available Widgets</p>
                            <ul id="widget_list" class="onit_widget_list">
                                <?php
                                foreach ($widgets as $id => $widget)
                                    echo "<li class='widget' data-id='{$id}'><div class='widget-top'><div class='widget-title'><h4>{$widget->name}</h4></div></div></li>";
                                ?>
                            </ul>


                            <?php


                        }, $screen, 'side', 'default');


                    endforeach;
                endif;
            }
        }
    }


    public
    function get_widget_lists()
    {
        global $wp_widget_factory;
        $widgets = array();
        if (is_array($wp_widget_factory->widgets)):
            foreach ($wp_widget_factory->widgets as $id => $widget):
                if (isset($widget->widget_category))
                    $widgets[$widget->widget_category][$id] = $widget;
                else
                    $widgets['Widgets'][$id] = $widget;
            endforeach;
        endif;

        return $widgets;
    }

    /**
     *    Public Method: Widget List
     *    Add Meta Box Callback to populate the list of available widgets for the widget area.
     */
    public
    function widget_list($widgets)
    {
        ?>
        <p>Available Widgets</p>
        <ul id="widget_list" class="onit_widget_list">
            <?php
            foreach ($widgets as $id => $widget)
                $this->widget_handle($widget, $id);
            ?>
        </ul>
        <?php
    }

    /**
     *    Private Method: Get Widget List
     *    Gets the list of available widget handles.
     */
    private
    function get_widget_list($widgets)
    {
        global $wp_widget_factory;

        if (is_array($wp_widget_factory->widgets))
            foreach ($wp_widget_factory->widgets as $id => $widget)
                $this->widget_handle($widget, $id);

        return;
    }

    /**
     *    Private Method: Widget handle
     *    Prints the widget handle.
     */
    private
    function widget_handle($widget, $id)
    {
        echo "<li class='widget' data-id='{$id}'><div class='widget-top'><div class='widget-title'><h4>{$widget->name}</h4></div></div></li>";

        return;
    }


    public
    function digit_area($post)
    {
        global $wp_widget_factory;
        include plugin_dir_path(dirname(__FILE__)) . 'partials/onnet-widget-js-template.php';
        ?>
        <div class="widgets_selected" id="widgets_selected">
            <p>Drag your widgets here</p>
            <ul class="digit_area drop_zone" id="digit_sortable" data-post_id= <?php echo $post->ID; ?>>
            </ul>
        </div>
        <?php
        // Widget Template
    }

    /**
     *    Public Method: Unregister Widgets.
     *    Removes the selected excluded widgets on the selected page editor pages.
     */
    public function unregister_widgets()
    {
        if (!is_admin())
            return;

        global $pagenow;

        if ($pagenow === 'widgets.php' || $pagenow === 'admin-ajax.php')
            return;

        if (array_key_exists('page', $_GET) && $_GET['page'] === 'onnet-digit-widgets')
            return;

        $exclude = get_option('onnet_digit_widget_options');
        
        if (!is_array($exclude))
            return;

        if(!array_key_exists('exclude_widgets', $exclude))
            return;

        foreach ($exclude['exclude_widgets'] as $widget)
            unregister_widget($widget);
    }
}