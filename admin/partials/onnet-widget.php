<?php

class OnNet_Widget
{

    // The Widget
    private $widget;

    // Widget - The widget key
    private $widget_key;

    // Widget Unique ID
    private $widget_id;

    // Widget Instance
    private $instance;

    // Post ID
    private $post_id;

    //Tabel
    private $table = 'digit';

    public function __construct($post_id, $widget_key, $widget_id, $instance = array())
    {
        // Set Post id
        $this->post_id = $post_id;

        // Set Widget Key
        $this->widget_key = $widget_key;

        // Set/Create Widget ID.
        if ($widget_id == false)
            $this->widget_id = $this->create();
        else
            $this->widget_id = $widget_id;

        // Set/Get Widget Instance
        $this->instance = $instance;

        // Create the widget
        if ($widget_key !== false)
        {
            $this->widget = new $this->widget_key;
            $this->widget->number = $this->widget_id;
            $this->widget->id = $this->widget_id;
        }
    }

    /**
     *    Private Method: Create Widget
     *    Saves the new widget instance to the database
     */
    private function create()
    {
        global $wpdb;

        // Create the widget record in the db.
        $data = array(
            'digit_key' => $this->widget_key,
            'post_id' => $this->post_id,
            'digit_value' => json_encode(array()),
        );

        // Insert row
        $wpdb->insert($wpdb->prefix . $this->table, $data);

        // Return widget id.
        return $wpdb->insert_id;
    }

    public function update($instance = array() )
    {
        global $wpdb;

        if( !is_array($instance) )
            $instance = array();

        $instance = $this->update_conditional_variables($instance);

        // Update data
        $table = $wpdb->prefix . $this->table;
        $update = array('digit_value' => json_encode($instance));
        $where = array('id' => $this->widget_id);
        
        // Update
        $wpdb->update($table, $update, $where);

        // Return new form
        return $this->get_form();
    }

    private function update_conditional_variables($instance)
    {
        // Onnet Digit conditional
        if ($this->widget_key === 'onnet_digit_widget')
            $instance['widgets'] = $this->get_digits_widgets();

        // Hyve lock not set conditional
        if( isset($instance['hyve_lock']) && $instance['hyve_lock'] === 'nolock')
            unset($instance['hyve_lock']);

        // Device demographic definition not set
        if( isset($instance['ddd']) && $instance['ddd'] === 'all')
            unset($instance['ddd']);

        return $instance;
    }

    private function get_digits_widgets()
    {
        $instance = (array)$this->get_instance();
        if (is_array($instance) && array_key_exists('widgets', $instance))
            return $instance['widgets'];

        return array();
    }

    public function update_digit_order($widgets)
    {
        global $wpdb;

        $instance = (array)$this->get_instance();

        $instance['widgets'] = array_filter($widgets);

        $table = $wpdb->prefix . $this->table;
        $update = array('digit_value' => json_encode($instance));
        $where = array('id' => $this->widget_id);

        // Update
        $wpdb->update($table, $update, $where);

    }

    /**
     *    Public Method Delete
     */
    public function delete()
    {
        global $wpdb;

        $delete = array('id' => $this->widget_id);

        $delete = $wpdb->delete($wpdb->prefix . $this->table, $delete);

        return $delete;
    }

    public function get_widget_data()
    {
        return array(
            'widget' => $this->widget_key,
            'name' => $this->widget->name,
            'title' => $this->get_instance_title(),
            'id' => $this->widget_id,
            'form' => $this->get_form(),
            'post_id' => $this->post_id,
        );
    }

    private function get_form()
    {
        ob_start();
        $this->widget->form($this->get_instance());
        $form = ob_get_clean();

        $old = 'widget-' . $this->widget->control_options['id_base'] . '[' . $this->widget->number . ']';

        // Rebase id control option
        $form = str_replace($old, $this->widget->number, $form);

        //temp remove..
        $form =  $this->digit_options() . $form;

        return $form;
    }

    private function digit_options()
    {
        // Hyve Lock
        $lock = $this->widget->number . '[hyve_lock]';
        if( $locked = isset($this->instance['hyve_lock'])?  $this->instance['hyve_lock'] : 'nolock' );
        if(  $locked !== 'nolock' || $hyve_active = '' )
            $hyve_active = 'active';

        // Device Detections
        $device = $this->widget->number . '[ddd]';
        if( $ddd = isset($this->instance['ddd'])?  $this->instance['ddd'] : 'all' );

        if(  $ddd !== 'all' || $ddd_active = '' )
            $ddd_active = 'active';

        // Header
        $header = "<div class='digit-options' id='digit-options--{$this->widget->number}'>";
        $header .= "<label class='digit_dd ddd-button {$ddd_active}' id='dd_{$this->widget->number}' title='Device Detection'></label>";
        $header .= "<label class='digit_lock lock-button {$hyve_active}' id='lock_{$this->widget->number}' title='Hyve lock'></label>";
        $header .= "</div>";


        // Container
        $content = "<div class='digit-option-content' id='digit-options--{$this->widget->number}-content'>";

        // Hyve Lock
        $content .= "<div id='lock_{$this->widget->number}-panel' class='digit_lock-panel'>";
        $content .= "<h4>Restrict this widgets visibility by Subscription Status</h4>";
        $content .= "<input type='radio' name='{$lock}' {$this->checked($locked, 'nolock')} value='nolock'><label> - Unlocked</label><br/>";
        $content .= "<input type='radio' name='{$lock}' {$this->checked($locked, 'subs')} value='subs'><label> - Subscribed Only</label><br/>";
        $content .= "<input type='radio' name='{$lock}' {$this->checked($locked, 'unsub')} value='unsub'><label> - No Subscriction Only</label><br/>";
        $content .= "</div>";

        // Device Lock
        $content .= "<div id='dd_{$this->widget->number}-panel' class='digit_dd-panel'>";
        $content .= "<h4>Restrict this widgets visibility by Device Type...</h4>";
        $content .= "<input type='radio' name='{$device}' {$this->checked($ddd, 'all')} value='all'><label> - All Devices</label><br/>";
        $content .= "<input type='radio' name='{$device}' {$this->checked($ddd, 'desktop')} value='desktop'><label> - Desktop Only</label><br/>";
        $content .= "<input type='radio' name='{$device}' {$this->checked($ddd, 'mobile')} value='mobile'><label> - Mobile Only</label><br/>";
        $content .= "<input type='radio' name='{$device}' {$this->checked($ddd, 'smart')} value='smart'><label> - Smart Phone Only</label><br/>";
        $content .= "<input type='radio' name='{$device}' {$this->checked($ddd, 'feature')} value='feature'><label> - Feature Phone Only</label><br/>";

        // End Container
        $content .= "</div>";

        return $header . $content;
    }

    private function checked($value, $check)
    {
        if( $value === $check )
            return "checked='checked'";
        else
            return "";
    }

    private function get_instance_title()
    {
        if (array_key_exists('title', $this->instance))
            return ': ' . stripslashes($this->instance['title']);

        return '';
    }

    public function get_instance()
    {
        global $wpdb;

        $table = $wpdb->prefix . $this->table;
        $query = "SELECT digit_value FROM {$table} WHERE id = '{$this->widget_id}'";

        $result = $wpdb->get_row($query);

        if (!$result)
            return array();

        $instance = (array)json_decode($result->digit_value);

        if (!is_array($instance))
            return array();


        foreach ($instance as $index => $row):
            if (!is_array($instance[$index]) && !is_object($instance[$index])):
                $instance[$index] = stripslashes($row);
            endif;
        endforeach;

        return $instance;
    }

}