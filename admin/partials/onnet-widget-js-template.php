<?php
/**
 * Js Templater.
 */
?>
<script type="text/template" id="widget-template">
<li class="widget" id="${id}" data-widget_key="${widget}">
	<div class="widget-top" >
		<div class="widget-title-action digit-action">
			<a class="widget-action hide-if-no-js" href="#digit_sortable"></a>
		</div>
		<div class="widget-title">
			<h4>${name}<span class="in-widget-title">${title}</span></h4>
		</div> 
	</div>
	<div class="widget-inside">
		<div class="widget-content">
			<form class="widget-form">
				{{html form}}
			</form>
		</div>
	 <div class="widget-control-actions">
		<div class="alignleft">
			<span class="digit-delete">Delete</span> |
			<span class="digit-close" href="#close">Close</span>
		</div>
		<div class="alignright">
			<span class="button button-primary widget-control-save right digit-save">Save</span><span class="spinner"></span>
		</div>
		<br class="clear">
	</div>
	</div>
</li>
</script>