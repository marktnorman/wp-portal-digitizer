(function ($) {
    /**
     *    Container Keys
     */
    var digits = get_digit_areas();

    // Define template
    $.template('widget', $('script#widget-template').html());

    /**
     *    Initial
     *    Runs on page load. Loops through all widgets and populates the widget area.
     */
    $(document).ready(function () {
        $.ajax({
            type: 'POST',
            url: digit.ajaxurl,
            data: {
                post_id: $('#digit_sortable').attr('data-post_id'),
                action: 'widgets_init'
            },
            dataType: 'json',
            success: function (data) {
                if (data !== false) {
                    $.each(data.order, function (i, id) {
                        create_widget_ui('#digit_sortable', data.widget[id]);
                    });
                }
                initialize();
            }
        });
    });


//.tmce-active


    /**
     *    Returns a list of all widget areas on the page.
     */
    function get_digit_areas() {
        var index = 1;
        var digits = new Array('#digit_sortable');

        $.each(digit.digits, function (i, digit) {
            digits[index] = digit;
            index++;
        });

        return digits;
    }

    /**
     *    initialize funciton.
     *    initializes all widget areas sortable, droppable functionality.
     */
    function initialize() {
        // Connect available widgets to digit areas.
        $('.onit_widget_list .widget').draggable({helper: "clone", scope: '.widget'});
        // Loop through each digit and initialize.
        $.each(digits, function (i, digit) {
            // Set up sortable.
            $(digit).sortable({
                items: '.widget', scope: '.widget',
                update: function (event, ui) {
                    update_widget_order(digit);
                },
                stop: function (event, ui) {
                    var widget_data = {
                        'name': $(ui.item.context).data('widget_key'),
                        'id': $(ui.item.context).attr('id')
                    };

                    $(document).trigger('digit-moved', widget_data);
                }
            }).disableSelection();

            // Set droppable
            $(digit).droppable(
                {
                    accept: '.widget',
                    helper: "clone",
                    greedy: true,
                    scope: '.widget',
                    // Hover Effect
                    over: function (event, ui) {
                        drop_toggle(digit, true);
                    },
                    // Remove Hover Effect
                    out: function (event, ui) {
                        drop_toggle(digit, false);

                    },
                    // Drop Create
                    drop: function (event, ui) {
                        drop_toggle(digit, false);

                        var target = '#' + $(event.target).attr('id');
                        var original = '#' + $(ui.helper).parent().attr('id');

                        // If Widget dropped or moved to new zone.
                        if (target !== original) {

                            var widget_data = {'target': target};

                            $(document).trigger('digit-dropped', widget_data);

                            if ($(ui.helper.context).attr('id')) {

                                move_widget(target, ui.helper, $(ui.helper.context).attr('id'), original);

                            } else {

                                create_widget(target, ui.helper);
                            }

                        }

                        if ($(ui.helper).attr('data-id') === 'onnet_digit_widget') {
                            initialize();
                        }
                    }
                });

        });
    }

    $(document).on('digit-dropped', function (e, widget) {
        $(widget.target).append("<li class='widget temp_widget'><span class='spinner is-active'></span><div class='widget-top'><div class='widget-title'><h4>Loading...</h4></div></div></li>");
    });

    $(document).on('digit-added', function (e, widget) {
        $('.temp_widget').remove();
    });

    /**
     *    Get Digit Areas
     *    Returns the string list of active digit areas on page
     */
    function get_digit_area_ids(exclude) {
        var digit_areas = digits;

        if (exclude !== false) {
            digit_areas = jQuery.grep(digits, function (value) {
                return value != exclude;
            });
        }
        return digit_areas.toString();
    }

    /**
     * Move Widget
     * @param target
     * @param widget
     * @param id
     * @param original
     */
    function move_widget(target, widget, id, original) {
        var old_widget = '#' + id;

        var widget_key = $(widget.context).data('widget_key');
        var widget_name = $(old_widget + ' > .widget-top .widget-title h4').text();
        var widget_title = $(old_widget + ' > .widget-top .in-widget-title').text();

        $.ajax({
            type: 'POST',
            url: digit.ajaxurl,
            data: {
                widget_id: id,
                widget: widget_key,
                post_id: $('#digit_sortable').attr('data-post_id'),
                data: $('#' + id + ' .widget-form').serialize(),
                action: 'update_widget'
            },
            dataType: 'json',
            beforeSend: function () {
                $(original + ' ' + old_widget).remove();
            },
            success: function (data) {

                var data = {'widget': widget_key, 'name': widget_name, 'title': widget_title, 'id': id, 'form': data};
                create_widget_ui(target, data);

                $(document).trigger('digit-added', data);

                update_widget_order(target);
                update_widget_order(original);
            }
        });

    }

    /**
     *
     *    Drop UX Toggle
     *
     */
    function drop_toggle(area, hover) {
        if (hover === true) {
            $(area).removeClass('drop_zone').addClass('hover_zone');
        } else {
            $(area).removeClass('hover_zone').addClass('drop_zone');
        }
    }

    /**
     *
     *    Create Widget
     *
     */
    function create_widget(area, widget) {
        var widget;

        $.ajax({
            type: 'POST',
            url: digit.ajaxurl,
            data: {
                widget: $(widget).attr('data-id'),
                post_id: $('#digit_sortable').attr('data-post_id'),
                action: 'create_widget',
            },
            dataType: 'json',
            success: function (data) {

                create_widget_ui(area, data);

                widget_toggle(data.id);

                update_widget_order(area);

                if (is_digit_widget(data.widget)) {

                    digits[digits.length] = '#d' + data.id;

                    initialize();
                }
            }
        });
    }

    // Widget Interface
    function create_widget_ui(area, data) {
        if (data !== undefined) {
            // Populate Widget template
            $.tmpl("widget", data).appendTo($(area));

            var widget_data = {'name': data.widget, 'id': data.id};
            $(document).trigger('digit-added', widget_data);

            // If digit populate
            if (is_digit_widget(data['widget'])) {

                populate_digits(data['id']);
            }
        }
    }

    // Populate Digits
    function populate_digits(digit_id) {
        var digit_widget = false;

        $.ajax({
            type: 'POST',
            url: digit.ajaxurl,
            data: {
                post_id: $('#digit_sortable').attr('data-post_id'),
                digit_id: digit_id,
                action: 'digits_init',
            },
            dataType: 'json',
            success: function (data) {

                if (data !== false) {

                    $.each(data.order, function (i, id) {

                        create_widget_ui('#d' + digit_id, data.widget[id]);

                        if (is_digit_widget(data.widget[id].widget)) {

                            digit_widget = true;
                        }
                    });
                }

                initialize();
            }
        });
    }

    /**
     *    Update Widget Order.
     *    On widget drop,order update, or delete run ajax call to update the widget
     *    location in the database.
     */
    function update_widget_order(widget_area) {
        $.ajax({
            type: 'POST',
            url: digit.ajaxurl,
            data: {
                widgets: get_widget_list(widget_area),
                post_id: $('#digit_sortable').attr('data-post_id'),
                widget_area: widget_area,
                action: 'widget_order'
            },
            dataType: 'json',
        });
    }

    /**
     *
     *    Get List of widgets in widget area.
     *
     */
    function get_widget_list(widget_area) {
        if ($(widget_area + ' > li').length === 0) {
            return ''; // empty list.
        }

        var widgets = new Array();
        $(widget_area + ' > li').each(function (i, e) {
            widgets[i] = $(e).attr('id');
        });

        return widgets;
    }

    /**
     *
     *    Toggle function
     *
     */
    function widget_toggle(widget_id) {
        widget_id = '#' + widget_id;

        if ($(widget_id).hasClass('active')) {
            $(widget_id + ' > .widget-inside').slideUp('slow');
            $(widget_id).removeClass('active');
        } else {
            $(widget_id + ' > .widget-inside').slideDown('slow');
            $(widget_id).addClass('active');
        }
    }

    // Widget Toggle Trigger.
    $(document).on('click', '.digit-action, .digit-close', function () {
        widget_toggle($(this).parents('.widget').attr('id'));
    });

    /**
     *
     *    Delete Widget
     *
     */
    function delete_widget(widget_id, widget_area) {
        $.ajax({
            type: 'POST',
            url: digit.ajaxurl,
            data: {
                widget: widget_id,
                post_id: $('#digit_sortable').attr('data-post_id'),
                action: 'delete_widget',
            },
            dataType: 'json',
            success: function (data) {
                if (data === 1) {
                    $('#' + widget_id).remove();
                    update_widget_order(widget_area);
                }
            }
        });
    }

    // Delete widget trigger.
    $(document).on('click', '.digit-delete', function () {
        var widget_id = $(this).parents('.widget').attr('id');
        var widget_key = $(this).parents('.widget').attr('data-widget_key');
        var widget_area = '#' + $(this).parents('.digit_area').attr('id');

        if (is_digit_widget(widget_key)) {
            confirmation = 'Are you sure you want to delete this multi widget?\n\n Note: All internal widgets will be deleted.';
        } else {
            confirmation = 'Are you sure you want to delete this widget?';
        }

        if (confirm(confirmation)) {
            if (is_digit_widget(widget_key)) {
                delete_digits(widget_id);
            }

            delete_widget(widget_id, widget_area);
        }
    });

    function is_digit_widget(widget_key) {
        if (widget_key === 'onnet_digit_widget') {
            return true;
        } else {
            return false;
        }

    }

    /**
     *    Delete Digits
     *    Recursivly delete all digits within digits.
     */
    function delete_digits(widget_area) {
        var widgets = get_widget_list('#d' + widget_area);
        if ($.isArray(widgets)) {
            $.each(widgets, function (i, widget_id) {
                var widget_key = $('#' + widget_id).attr('data-widget_key');
                if (is_digit_widget(widget_key)) {
                    delete_digits(widget_id);
                }
                delete_widget(widget_id, widget_area);
            });
        }
    }

    /**
     *    Update Widget
     *
     */
    function update_widget(widget, widget_id) {

        $.ajax({
            type: 'POST',
            url: digit.ajaxurl,
            data: {
                widget_id: widget_id,
                widget: widget,
                post_id: $('#digit_sortable').attr('data-post_id'),
                data: $('#' + widget_id + ' .widget-form').serialize(),
                action: 'update_widget'
            },
            dataType: 'json',
            beforeSend: function () {
                $('#' + widget_id + ' .spinner').addClass('is-active')
            },
            success: function (data) {

                $('#' + widget_id + ' .spinner').removeClass('is-active');
                $('#' + widget_id + ' .widget-form').empty().append(data);

                if (is_digit_widget(widget)) {
                    populate_digits(widget_id);
                }

                // Trigger
                $(document).trigger('digit-updated', {'name': widget, 'id': widget_id});

                if (widget === 'onnet_digit_widget') {
                    initialize();
                }
            },
        });
    }

    // Update Widget Trigger
    $(document).on('click', '.digit-save', function () {

        var widget, widget_id;

        widget = $(this).parents('.widget').attr('data-widget_key');
        widget_id = $(this).parents('.widget').attr('id');

        var widget_data = {'name': widget, 'id': widget_id};
        $(document).trigger('digit-save', widget_data);

        update_widget(widget, widget_id);
    });


    // Device Detection Panel
    $(document).on('click', '.ddd-button', function () {
        var id = '#' + $(this).parent().attr('id');

        if ($(id).hasClass('active-options')) {
            $(id + ' .digit_lock').removeClass('lock-close').addClass('lock-button');
            $(id + '-content .digit_lock-panel').hide();
        }
        $(this).removeClass('ddd-button').addClass('ddd-close');
        $(id).addClass('active-options');
        var panel = '#' + $(this).attr('id') + '-panel';
        $(panel).show();

    });

    $(document).on('click', '.ddd-close', function () {
        $(this).removeClass('ddd-close').addClass('ddd-button');
        $('.digit-options').removeClass('active-options');
        var panel = '#' + $(this).attr('id') + '-panel';
        $(panel).hide();
    });


    //Hyve Lock
    $(document).on('click', '.lock-button', function () {
        var id = '#' + $(this).parent().attr('id');

        if ($(id).hasClass('active-options')) {
            $(id + ' .digit_dd').removeClass('ddd-close').addClass('ddd-button');
            $(id + '-content .digit_dd-panel').hide();
        }

        $(this).removeClass('lock-button').addClass('lock-close');
        $(id).addClass('active-options');
        var panel = '#' + $(this).attr('id') + '-panel';
        $(panel).show();
    });

    $(document).on('click', '.lock-close', function () {
        $(this).removeClass('lock-close').addClass('lock-button');
        $('.digit-options').removeClass('active-options');
        var panel = '#' + $(this).attr('id') + '-panel';
        $(panel).hide();
    });

    /**
     * Widget Setting Panel UI.
     */
    $(document).on('click', '#onnet_digit .widget-title-action', function () {
        var id = '#' + $(this).attr('id').replace('--toggle', '');

        if ($(id + ' .widget-inside').hasClass('active')) {
            $(id + ' .widget-inside').removeClass('active');
        } else {
            $(id + ' .widget-inside').addClass('active');
        }
    });


    // WYSIWYG
    $(document).on('digit-save', function (e, widget) {
        if ($('#' + widget.id + ' .wysiwyg_block').length > 0) {
            $.each($('#' + widget.id + ' .wysiwyg_block'), function (i, element) {
                tinymce.execCommand('mceRemoveEditor', true, $(element).attr('id').replace('--wrapper', ''));
            });
        }
    });

    $(document).on('digit-added', function (e, widget) {
        if ($('#' + widget.id + ' .wysiwyg_block').length > 0) {
            $.each($('#' + widget.id + ' .wysiwyg_block'), function (i, element) {
                tiny_mce_init($(element).attr('id').replace('--wrapper', ''));
            });
        }
    });

    $(document).on('digit-updated', function (e, widget) {
        if ($('#' + widget.id + ' .wysiwyg_block').length > 0) {
            $.each($('#' + widget.id + ' .wysiwyg_block'), function (i, element) {
                tiny_mce_init($(element).attr('id').replace('--wrapper', ''));
            });
        }
    });

    $(document).on('digit-moved', function (e, widget) {

        if ($('#' + widget.id + ' .wysiwyg_block').length > 0) {
            $.each($('#' + widget.id + ' .wysiwyg_block'), function (i, element) {
                tinymce.execCommand('mceRemoveEditor', true, $(element).attr('id').replace('--wrapper', ''));
                tiny_mce_init($(element).attr('id').replace('--wrapper', ''));
            });
        }
    });

    /**
     *    Tiny MCE Init
     *    Set up the tiny mce widget.
     */
    function tiny_mce_init(id) {
        tinymce.init({
            // skin: 'wp_editor',
            mode: "specific_textareas",
            theme: 'modern',
            editor_selector: id,
            skin: "lightgray",
            menu: {
                tools: 'italic underline'

            },
        });
    }


})(jQuery);