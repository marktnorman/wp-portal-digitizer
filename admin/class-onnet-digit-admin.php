<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://onnet.co.za/
 * @since      1.0.0
 *
 * @package    Onnet_Digit
 * @subpackage Onnet_Digit/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Onnet_Digit
 * @subpackage Onnet_Digit/admin
 * @author     OnNet <code@onnet.co.za>
 */
class Onnet_Digit_Admin
{

    /**
     * The ID of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string $plugin_name The ID of this plugin.
     */
    private $plugin_name;

    /**
     * The version of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string $version The current version of this plugin.
     */
    private $version;

    /**
     * Initialize the class and set its properties.
     *
     * @since    1.0.0
     *
     * @param      string $plugin_name The name of this plugin.
     * @param      string $version The version of this plugin.
     */
    public function __construct($plugin_name, $version)
    {
        $this->plugin_name = $plugin_name;
        $this->version = $version;

        $this->options = get_option('onnet_digit_options');
        $this->widgets = get_option('onnet_digit_widget_options');

        $widget_class = new Onnet_widgets;

        add_action('delete_post', array($this, 'delete_widgets_in_post'));
    }


    /**
     * Register the stylesheets for the admin area.
     *
     * @since    1.0.0
     */
    public function enqueue_styles()
    {

        wp_enqueue_style($this->plugin_name, plugin_dir_url(__FILE__) . 'css/onnet-digit-admin.css', array(), $this->version, 'all');

        wp_enqueue_style('digit-css', plugin_dir_url(__FILE__) . 'css/the_digit.css', array(), $this->version, 'all');

        wp_enqueue_style('jquery_ui_css', 'https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css', $this->version, true);

    }


    public function digit_table()
    {
        global $wpdb;
        $charset_collate = !empty($wpdb->charset) ? "DEFAULT CHARACTER SET {$wpdb->charset}" : '';
        $charset_collate .= !empty($wpdb->collate) ? " COLLATE {$wpdb->collate}" : '';

        // Check that the table does not exist already
        $table_sql = "CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}digit` (
                `id` bigint(20) NOT NULL AUTO_INCREMENT,
                `post_id` bigint(20) NOT NULL,
                `digit_key` VARCHAR(255) NOT NULL,
                `digit_value` LONGTEXT DEFAULT NULL,
                PRIMARY KEY  (id)
            )
            ENGINE=InnoDB {$charset_collate};";

        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

        dbDelta($table_sql);
    }


    /**
     *    Delete Post
     *    Hook into delete post and remove all widgets from the post.
     */
    public function delete_widgets_in_post()
    {
        global $wpdb;
        global $post;

        if (!is_object($post))
            return;

        if (!property_exists($post, 'ID'))
            return;

        $results = $wpdb->get_results("SELECT id FROM {$wpdb->prefix}digit WHERE post_id = {$post->ID}", OBJECT_K);

        if (!is_array($results) || empty($results))
            return;

        $results = array_keys($results);

        foreach ($results as $id)
            $delete = $wpdb->delete($wpdb->prefix . 'digit', array('id' => $id));
    }


    /**
     * Register the JavaScript for the admin area.
     *
     * @since    1.0.0
     */
    public function enqueue_scripts()
    {
        wp_enqueue_script('jquery-ui-draggable');
        wp_enqueue_script('jquery-ui-droppable');
        wp_enqueue_script('jquery-ui-sortable');
        wp_enqueue_script('tinymce', includes_url() . 'js/tinymce/wp-tinymce.php', array('jquery'), $this->version, false);
        wp_enqueue_script('tmpl', plugin_dir_url(__FILE__) . 'js/jquery.tmpl.js', array('jquery'), $this->version, false);
        wp_enqueue_script($this->plugin_name, plugin_dir_url(__FILE__) . 'js/onnet-digit-admin.js', array('jquery'), $this->version, false);

        wp_register_script('digit-js', plugin_dir_url(__FILE__) . 'js/digit.js', array('jquery-ui-draggable', 'jquery-ui-droppable', 'jquery-ui-sortable', 'tmpl'), $this->version, true);

        $data = array(
            'ajaxurl' => admin_url('admin-ajax.php'),
            'digits' => $this->get_all_digits()
        );

        wp_localize_script('digit-js', 'digit', $data);

        wp_enqueue_script('digit-js');
    }

    private function get_all_digits()
    {
        global $wpdb;
        global $post;

        $ids = array();

        if ($post === null)
            return array();

        $query = "SELECT id FROM {$wpdb->prefix}digit WHERE digit_key = 'onnet_digit_widget' AND post_id ={$post->ID}";
        $results = $wpdb->get_results($query);

        if (!$results)
            return $ids;

        if (is_array($results))
            foreach ($results as $row)
                $ids[] = '#d' . $row->id;

        return $ids;
    }


    /**
     * Register the admin menu
     */
    public function admin_menu()
    {

        /**
         *
         * add_submenu_page($this->plugin_name . '-post_types', __('All Post Types', $this->plugin_name), __('All Post Types', $this->plugin_name), 'manage_options', $this->plugin_name . '-post_types', array($this, 'post_types_listing'));
         * add_submenu_page($this->plugin_name . '-post_types', __('All Taxonomies', $this->plugin_name), __('All Taxonomies', $this->plugin_name), 'manage_options', $this->plugin_name . '-taxonomies', array($this, 'taxonomies_listing'));
         *
         */
        $capability = 'manage_options';

        add_menu_page(__('Digit'), __('Digit'), $capability, $this->plugin_name . '-settings', array(
            $this, 'render_settings'
        ), plugin_dir_url(__FILE__) . 'img' . DIRECTORY_SEPARATOR . 'icon.png');


        add_submenu_page($this->plugin_name . '-settings', __('Settings'), __('Settings'), $capability, $this->plugin_name . '-settings', array(
            $this, 'render_settings'
        ));

        add_submenu_page($this->plugin_name . '-settings', __('Widgets'), __('Widgets'), $capability, $this->plugin_name . '-widgets', array(
            $this, 'render_widgets'
        ));
        remove_menu_page('edit.php?post_type=digitizer');

        add_submenu_page($this->plugin_name . '-settings', __('Digitizer'), __('Digitizer'), $capability, 'edit.php?post_type=digitizer');

    }


    public function tab_navigation()
    {

        if (isset($_GET['page']) && $_GET['page'] == 'onnet-digit-widgets') {
            $active_tab = 'widgets';
        } elseif (isset($_GET['page']) && $_GET['page'] == 'onnet-digit-settings') {
            $active_tab = 'settings';
        }
        ?>

        <h2 class="nav-tab-wrapper">
            <a href="?page=onnet-digit-settings&amp;tab=settings"
               class="nav-tab <?php echo $active_tab == 'settings' ? 'nav-tab-active' : ''; ?>"><?php _e('Settings', 'onnet'); ?></a>
            <a href="?page=onnet-digit-widgets&amp;tab=widgets"
               class="nav-tab <?php echo $active_tab == 'widgets' ? 'nav-tab-active' : ''; ?>"><?php _e('Widgets', 'onnet'); ?></a>
            <a href="edit.php?post_type=digitizer"
               class="nav-tab"><?php _e('Digitizer', 'onnet'); ?></a>
        </h2>

        <?php

    }


    public function render_settings()
    {

        ?>
        <div class="wrap" id="onnet_digit">

            <h2><?php echo __('Settings', 'onnet'); ?></h2>

            <?php $this->tab_navigation(); ?>

            <form method="post" action="options.php">

                <div id="poststuff" class="">
                    <div class="postbox">
                        <?php
                        // This prints out all hidden setting fields
                        settings_fields($this->plugin_name . '_option_group');
                        do_settings_sections($this->plugin_name . '_settings_admin');
                        submit_button();
                        ?>
                    </div>
                </div>
            </form>
        </div>
        <?php

    }


    public function render_widgets()
    {

        ?>
        <div class="wrap" id="onnet_digit">

            <h2><?php echo __('Widgets', 'onnet'); ?></h2>

            <?php $this->tab_navigation(); ?>

            <form method="post" action="options.php">
                <div id="poststuff" class="">
                    <div class="postbox">
                        <?php
                        // This prints out all hidden setting fields
                        settings_fields($this->plugin_name . '_option_widgets');
                        do_settings_sections($this->plugin_name . '_widgets_admin');
                        submit_button();
                        ?>
                    </div>
                </div>
            </form>

        </div>
        <?php
    }

    public function print_section_info()
    {
        return;
    }

    /**
     * Register and add settings
     */
    public function page_init()
    {

        // Settings Page
        register_setting(
            $this->plugin_name . '_option_group', // Option group
            'onnet_digit_options', // Option name
            array($this, 'sanitize') // Sanitize
        );

        add_settings_section(
            'setting_section_id', // ID
            '', // Title
            array($this, 'print_section_info'), // Callback
            $this->plugin_name . '_settings_admin' // Page
        );

        add_settings_field(
            'digit_post_types',
            'Post Types',
            array($this, 'digit_post_types_callback'),
            $this->plugin_name . '_settings_admin',
            'setting_section_id'
        );


        // Widget Page
        register_setting(
            $this->plugin_name . '_option_widgets', // Option group
            'onnet_digit_widget_options', // Option name
            array($this, 'sanitize') // Sanitize
        );

        add_settings_section(
            'widgets_section_id', // ID
            '', // Title
            array($this, 'print_section_info'), // Callback
            $this->plugin_name . '_widgets_admin' // Page
        );

        add_settings_field(
            'exclude_widgets',
            'Exclude Widgets',
            array($this, 'exclude_widgets_callback'),
            $this->plugin_name . '_widgets_admin',
            'widgets_section_id'
        );

    }


    /**
     * Sanitize each setting field as needed
     *
     * @param array $input Contains all settings fields as array keys
     */
    public function sanitize($input)
    {

        $new_input = array();


        // Settings Page
        if (isset($input['digit_post_types'])) {
            $new_input['digit_post_types'] = $input['digit_post_types'];
        }


        // Widgets Page
        if (isset($input['exclude_widgets'])) {
            $new_input['exclude_widgets'] = $input['exclude_widgets'];
        }

        return $new_input;
    }


    // Settings Page
    public function digit_post_types_callback()
    {

        $args = array(
            'public' => true
        );

        $postoutput = 'names';
        $post_types = get_post_types($args, 'names');
        $saved = (isset($this->options['digit_post_types'])) ? $this->options['digit_post_types'] : '';

        echo '<div class="digit-post-types">';
        if (!empty($post_types)) {
            foreach ($post_types as $post_type) {
                if (isset($post_type) && $post_type != 'attachment') {

                    if (isset($saved) && $saved != "" && in_array($post_type, $saved)) {
                        $checked = 'checked="checked"';
                    } else {
                        $checked = '';
                    }

                    echo '<input type="checkbox" id="digit_post_types" name="onnet_digit_options[digit_post_types][]" value="' . $post_type . '" ' . $checked . ' /> ' . $post_type . '<br />';

                }

            }
        }
        echo "</div>";

    }

    // Widgets Page
    public function exclude_widgets_callback()
    {
        global $wp_widget_factory;

        if (empty($wp_widget_factory->widgets))
            return;

        $saved = (isset($this->widgets['exclude_widgets'])) ? $this->widgets['exclude_widgets'] : array();

        $size = count($wp_widget_factory->widgets);
        $half = ($size % 2) + ($size / 2);

        $widgets = (array_chunk($wp_widget_factory->widgets, $half, true));

        if (!is_array($widgets))
            return;

        foreach ($widgets as $column_widget):
            echo "<div class = 'column_widget'>";

            $size = count($column_widget);
            $half = ($size % 2) + ($size / 2);
            $column_widget = array_chunk($column_widget, $half, true);

            if (is_array($column_widget) && !empty ($column_widget)):

                foreach ($column_widget as $column):
                    echo "<div class = 'column_widget--inner'>";

                    foreach ($column as $name => $widget):
                        $checked = (in_array($name, $saved)) ? $checked = "checked='checked'" : false;
                        ?>
                        <div id="<?php echo $name ?>"
                             class="widget widget-exclude <?php echo ($checked) ? 'opacity-widget' : '' ?>">
                            <div class="widget-top">
                                <div class="widget-title-action exclude-action" id='<?php echo $name; ?>--toggle'>
                                    <a class="widget-action hide-if-no-js" href="#<?php echo $name; ?>"></a>
                                </div>
                                <div class="widget-title ui-sortable-handle"><h3><?php echo $widget->name; ?><span
                                            class="in-widget-title"></span></h3></div>
                            </div>
                            <div class="widget-inside" id="<?php echo $name; ?>--inside">
                                <p><b>Description</b></p>
                                <?php echo $widget->widget_options['description']; ?><br/><br/>
                                <input id='exclude_widgets' type='checkbox'
                                       name='onnet_digit_widget_options[exclude_widgets][]'
                                       value='<?php echo $name; ?>' <?php echo $checked; ?>> Unregister this widget<br/><br/>
                            </div>
                        </div>
                        <?php

                    endforeach;
                    echo "</div>";
                endforeach;
            endif;

            echo "</div>";
        endforeach;
    }


}